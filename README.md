**-- Database --**


For this solution I have used H2 in memory database. The configurations are provided in application.properties file.
The database tables can be reached with the link:

_localhost:8080/h2-console/_

The table contains 4 columns: ID, content, timestamp and longestPalindromeSize (which is calculated in PalidromeService)

**Testing the solution**

To test the RESP endpoint, I have used Postman with the following configurations: 

![](src/main/resources/Images/1.png)

After the request is sent, the palidrome value is being calculated. 
The values can be retrieved from database with GET request with the link:
http://localhost:8080/list

The result: 

![](src/main/resources/Images/2.png)

**Error Handling**

I have created a controller for error handling. The 'static' folder contains 2 templates. In case of handling more error tempates the Thymeleaf template engine can be used for creating a custom template with dynamic values e.g. error code, error message. 