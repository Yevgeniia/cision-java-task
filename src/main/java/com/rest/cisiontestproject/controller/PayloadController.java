package com.rest.cisiontestproject.controller;

import com.rest.cisiontestproject.model.Payload;
import com.rest.cisiontestproject.service.PalindromeService;
import com.rest.cisiontestproject.service.PayloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class PayloadController {
    @Autowired
    private PayloadService payloadService;
    @Autowired
    private PalindromeService palindromeService;

    @RequestMapping(
            value = "/getPayload",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getPayload(@Valid @RequestBody Payload payload, Errors errors) {

        //Handling error in case of a bad request
        if (errors.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            int result = palindromeService.calculateLongestPalindrome(payload.getContent());

            if (result != 0) {
                Payload p = new Payload(payload.getContent(), payload.getTimestamp(), result);
                payloadService.save(p);
            }
            return new ResponseEntity<>(HttpStatus.OK);
        }

    }

    @RequestMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Iterable<Payload> list() {
        return payloadService.list();
    }
}
