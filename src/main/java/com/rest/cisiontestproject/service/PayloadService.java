package com.rest.cisiontestproject.service;

import com.rest.cisiontestproject.model.Payload;
import com.rest.cisiontestproject.repository.PayloadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PayloadService {

    @Autowired
    private PayloadRepository payloadRepository;

    public Payload save(Payload payload) {
        return payloadRepository.save(payload);
    }

    public Iterable<Payload> list() {
        return payloadRepository.findAll();
    }

}
