package com.rest.cisiontestproject.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class PalindromeService {

    public PalindromeService() {
    }

    public int calculateLongestPalindrome(String text) {
        List<String> charSetsToCheck = new ArrayList<>();
        String charSet = "";
        for (int t = 0; t < text.length(); t++) {
            if (Character.isDigit(text.charAt(t))) {
                if(!charSet.equals("")){
                    charSetsToCheck.add(charSet);
                }
                charSet = "";
            } else {
                charSet = charSet.concat(String.valueOf(text.charAt(t)));
                if (t == text.length() - 1) {
                    charSetsToCheck.add(charSet);
                }
            }
        }

        List<Integer> allPalidromeValues = new ArrayList<>();

        if (charSetsToCheck.size() != 0) {
            charSetsToCheck.forEach(txt -> {
                int n = findPalindromeFromText(txt);
                allPalidromeValues.add(n);
            });

        } else {
            return 0;
        }

        return Collections.max(allPalidromeValues);
    }

    private int findPalindromeFromText(String text) {

        int maxLength = 1;
        int start = 0;

        for (int i = 0; i < text.length(); i++) {
            for (int j = i; j < text.length(); j++) {
                int flag = 1;

                for (int k = 0; k < (j - i + 1) / 2; k++) {
                    flag = text.charAt(i + k) != text.charAt(j - k) ? 0 : flag;
                }

                if (flag != 0 && (j - i + 1) > maxLength) {
                    start = i;
                    maxLength = j - i + 1;
                }
            }
        }
        return maxLength;
    }
}
