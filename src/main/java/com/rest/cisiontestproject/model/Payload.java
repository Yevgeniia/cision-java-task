package com.rest.cisiontestproject.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
public class Payload {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NotBlank
    private String content;

    @Column
    @NotBlank
    private String timestamp;

    @Column
    private int longestPalindromeSize;

    public Payload(String content, String timestamp, int longestPalindromeSize) {
        this.content = content;
        this.timestamp = timestamp;
        this.longestPalindromeSize = longestPalindromeSize;
    }

    public Payload() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getLongestPalindromeSize() {
        return longestPalindromeSize;
    }

    public void setLongestPalindromeSize(int longestPalindromeSize) {
        this.longestPalindromeSize = longestPalindromeSize;
    }
}
