package com.rest.cisiontestproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CisionTestProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(CisionTestProjectApplication.class, args);
    }

}
